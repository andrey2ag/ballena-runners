package cr.uvita.ballenarunners;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ZonasFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ZonasFragment extends Fragment {

    private View myView;

    public ZonasFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static ZonasFragment newInstance(String param1, String param2) {
        ZonasFragment fragment = new ZonasFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.fragment_zonas, container, false);

        getUI();


        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcularResultado();
                UIUtils.hideKeyboard(getActivity());
            }
        });

        btnCalcularPulso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcularPulso();
                UIUtils.hideKeyboard(getActivity());
            }
        });

//        btnDistance.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(BasicActivity.this, MainActivity.class);
//                startActivity(intent);
//            }
//        });



        return myView;
    }

    private EditText edHoras;
    private EditText edMinutos;
    private EditText edSegundos;

    private ImageView btnDistance;

    private EditText edPorcentaje;
    private TextView tvZonas;

    private Button btnCalcular;
    private TextView lbResultado;

    private Spinner spMedidas;

    private EditText edPulsaciones;

    private Button btnCalcularPulso;
    private TextView lbResultadoPulso;


    private void calcularPulso() {
        int pulsaciones = getValue(edPulsaciones);

        int pulso = pulsaciones * 6;

        lbResultadoPulso.setText("Pulso: " + pulso);

    }

    private void calcularResultado() {

        int hours = getValue(edHoras);
        int minutes = getValue(edMinutos);
        int seconds = getValue(edSegundos);

        int totalSeconds = hoursToSeconds(hours) + minutesToSeconds(minutes) + seconds;

        int porcentaje = getValue(edPorcentaje);


        double totalSecs = totalSeconds + (totalSeconds - (totalSeconds * (porcentaje / 100.0)));
        String timeString = secondsToTimeFormat(totalSecs);
        String result1 = String.format("▶ %d%%: %s", porcentaje,timeString);
        lbResultado.setText(result1);


        if (porcentaje >= 50 && porcentaje < 60){
            tvZonas.setText("AER 50%-60%");
        }else if (porcentaje >= 60 && porcentaje < 70){
            tvZonas.setText("AEL 60%-70%");
        }else if (porcentaje >= 70 && porcentaje < 80){
            tvZonas.setText("AEM 70%-80%");
        }else if (porcentaje >= 80 && porcentaje < 90){
            tvZonas.setText("AEI 80%-90%");
        }else if (porcentaje >= 90 && porcentaje <= 100){
            tvZonas.setText("ANA 90%-100%");
        }else{
            tvZonas.setText("");
        }




    }

    private String secondsToTimeFormat(double totalSecs) {
        int fhours = (int) totalSecs / 3600;
        int fminutes = (int) (totalSecs % 3600) / 60;
        int fseconds = (int) totalSecs % 60;

        return String.format("%02dh %02dm %02ds", fhours, fminutes, fseconds);
    }

    private int getValue(EditText ed) {
        if (!ed.getText().toString().equals("")) {
            return Integer.parseInt(ed.getText().toString());
        } else {
            ed.setText("0");
            return 0;
        }
    }

    private int getValue(EditText ed, int defaultValue) {
        if (!ed.getText().toString().equals("")) {
            return Integer.parseInt(ed.getText().toString());
        } else {
            ed.setText(String.valueOf(defaultValue));
            return defaultValue;
        }
    }

    private int hoursToSeconds(int hours) {
        return hours * 60 * 60;
    }

    private int minutesToSeconds(int minutes) {
        return minutes * 60;
    }

    private void getUI() {
        edHoras = (EditText) myView.findViewById(R.id.edHoras);
        edMinutos = (EditText) myView.findViewById(R.id.edMinutos);
        edSegundos = (EditText) myView.findViewById(R.id.edSegundos);

        edPorcentaje = (EditText) myView.findViewById(R.id.edPorcentaje);
        tvZonas = (TextView) myView.findViewById(R.id.tvZonas);

        spMedidas =  (Spinner) myView.findViewById(R.id.spMedida);

        btnCalcular = (Button) myView.findViewById(R.id.btnCalcular);
        lbResultado = (TextView) myView.findViewById(R.id.lbResultado);


        edPulsaciones = (EditText) myView.findViewById(R.id.edPulsaciones);
        btnCalcularPulso = (Button) myView.findViewById(R.id.btnCalcularPulso);
        lbResultadoPulso = (TextView) myView.findViewById(R.id.lbResultadoPulso);

//        btnDistance = (ImageView) myView.findViewById(R.id.btnDistance);

    }



}
