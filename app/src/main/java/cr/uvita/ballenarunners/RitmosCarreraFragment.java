package cr.uvita.ballenarunners;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RitmosCarreraFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link RitmosCarreraFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RitmosCarreraFragment extends Fragment {

    private View myView;


    public RitmosCarreraFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static RitmosCarreraFragment newInstance(String param1, String param2) {
        RitmosCarreraFragment fragment = new RitmosCarreraFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.fragment_ritmos_carrera, container, false);
        getUI();


        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcularResultado();
                UIUtils.hideKeyboard(getActivity());
            }
        });

        return myView;

    }

    private EditText edHoras;
    private EditText edMinutos;
    private EditText edSegundos;

    private EditText edDistancia;

    private EditText edPorcentaje;

    private Button btnCalcular;
    private TextView lbResultado;
    private TextView lbResultado2;
    private TextView lbResultado3;
    private TextView lbResultado4;

    private Spinner spMedidas;


    private void calcularResultado() {

        int hours = getValue(edHoras);
        int minutes = getValue(edMinutos);
        int seconds = getValue(edSegundos);

        int distancia = getValue(edDistancia,1);

        int totalSeconds = hoursToSeconds(hours) + minutesToSeconds(minutes) + seconds;

        int porcentaje = getValue(edPorcentaje);

        String medidaDistancia = spMedidas.getSelectedItem().toString();

        double totalSecs = totalSeconds + (totalSeconds - (totalSeconds * (porcentaje / 100.0)));
        String timeString = secondsToTimeFormat(totalSecs);
        String result1 = String.format("▶ %d %s al %d%%: %s",distancia, medidaDistancia, porcentaje,timeString);
        lbResultado.setText(result1);

        if (medidaDistancia.equals("km") && distancia > 1) {
            timeString = secondsToTimeFormat(totalSeconds / distancia);
            String result2 = String.format("▶ Promedio x km al 100%%: %s", timeString);
            lbResultado2.setText(result2);

            timeString = secondsToTimeFormat(totalSecs/distancia);
            String result3 = String.format("▶ Promedio x km al %d%%: %s", porcentaje,timeString);
            lbResultado3.setText(result3);

        }else{
            lbResultado2.setText("");
            lbResultado3.setText("");
        }

    }

    private String secondsToTimeFormat(double totalSecs) {
        int fhours = (int) totalSecs / 3600;
        int fminutes = (int) (totalSecs % 3600) / 60;
        int fseconds = (int) totalSecs % 60;

        return String.format("%02dh %02dm %02ds", fhours, fminutes, fseconds);
    }

    private int getValue(EditText ed) {
        if (!ed.getText().toString().equals("")) {
            return Integer.parseInt(ed.getText().toString());
        } else {
            ed.setText("0");
            return 0;
        }
    }

    private int getValue(EditText ed, int defaultValue) {
        if (!ed.getText().toString().equals("")) {
            return Integer.parseInt(ed.getText().toString());
        } else {
            ed.setText(String.valueOf(defaultValue));
            return defaultValue;
        }
    }

    private int hoursToSeconds(int hours) {
        return hours * 60 * 60;
    }

    private int minutesToSeconds(int minutes) {
        return minutes * 60;
    }

    private void getUI() {
        edHoras = (EditText) myView.findViewById(R.id.edHoras);
        edMinutos = (EditText) myView.findViewById(R.id.edMinutos);
        edSegundos = (EditText) myView.findViewById(R.id.edSegundos);

        edPorcentaje = (EditText) myView.findViewById(R.id.edPorcentaje);
        edDistancia = (EditText) myView.findViewById(R.id.edDistancia);

        spMedidas =  (Spinner) myView.findViewById(R.id.spMedida);

        btnCalcular = (Button) myView.findViewById(R.id.btnCalcular);
        lbResultado = (TextView) myView.findViewById(R.id.lbResultado);
        lbResultado2 = (TextView) myView.findViewById(R.id.lbResultado2);
        lbResultado3 = (TextView) myView.findViewById(R.id.lbResultado3);
        lbResultado4 = (TextView) myView.findViewById(R.id.lbResultado4);


    }



}
